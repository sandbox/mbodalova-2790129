INTRODUCTION
------------

Module Project version provides settings to show project version or project
revision at the status page. You can select the source for showing project
version on status page: from file(txt) or custom version from the field.

REQUIREMENTS
------------
This module does not require the anything modules.

RECOMMENDED MODULES
-------------------


INSTALLATION
------------
* Copy the Project version directory to the modules folder in your
installation.

* Enable the module using Administer -> Modules (/admin/modules)

* Go to page /admin/config/system/project-version/settings and add settings.


CONFIGURATION
-------------
* After enable module, configure settings here:
  Administration »  Configuration » Development » Settings for Project version


TROUBLESHOOTING
---------------


FAQ
---


MAINTAINERS
-----------
Current maintainers:
* Maria Bodalova (MerryHamster) - https://www.drupal.org/u/merryhamster


This project has been sponsored by:
* Adyax  https://www.drupal.org/adyax
Visit https://www.adyax.com/ for more information.
