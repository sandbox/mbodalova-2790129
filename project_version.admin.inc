<?php

/**
 * @file
 * Admin page callbacks for the Project version module.
 */

/**
 * Callback for configuration form.
 *
 * @return mixed
 *   Return settings configuration form.
 */
function project_version_form($form, &$from_state) {

  $active = array(
    0 => t('Do not show project version'),
    1 => t('Show project version from field'),
    2 => t('Show project version from file'),
  );

  $form['project_version']['options'] = array(
    '#type' => 'radios',
    '#title' => t('Select a variant for show project version'),
    '#default_value' => variable_get('project_version_option', 0),
    '#options' => $active,
    '#required' => TRUE,
  );
  $form['project_version']['custom_version'] = array(
    '#type' => 'textfield',
    '#title' => t('Write version for show on status page'),
    '#description' => t('For example develop-3.8.'),
    '#size' => 60,
    '#maxlength' => 255,
    '#default_value' => variable_get('project_version_custom', ''),
    '#states' => array(
      'visible' => array(
        ':input[name="options"]' => array('value' => '1'),
      ),
    ),
  );
  $form['project_version']['file_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Write path to file with version'),
    '#description' => t('For example REVISION.txt or sites/default/REVISION.txt etc.'),
    '#size' => 60,
    '#maxlength' => 255,
    '#default_value' => variable_get('project_version_file_path', ''),
    '#states' => array(
      'visible' => array(
        ':input[name="options"]' => array('value' => '2'),
      ),
    ),
  );
  $form['project_version']['path_line'] = array(
    '#type' => 'textfield',
    '#title' => t('Write number line in file'),
    '#description' => t('Warning: first line = 0, second line = 1 etc.'),
    '#size' => 2,
    '#maxlength' => 2,
    '#default_value' => variable_get('project_version_path_line', ''),
    '#states' => array(
      'visible' => array(
        ':input[name="options"]' => array('value' => '2'),
      ),
    ),
  );
  $form = system_settings_form($form);
  $form['#validate'][] = 'project_version_admin_form_validate';
  $form['#submit'][] = 'project_version_admin_form_submit';
  return $form;
}

/**
 * Validate form with settings.
 */
function project_version_admin_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['options'])) {
    $option = $form_state['values']['options'];
    switch ($option) {
      case 1:
        if (empty($form_state['values']['custom_version'])) {
          form_set_error('custom_version', t('Please, write custom vertion.'));
        }
        break;

      case 2:
        $file_path = $form_state['values']['file_path'];
        $exists = file_exists($file_path);
        $readable = is_readable($file_path);
        if (empty($form_state['values']['file_path'])) {
          form_set_error('file_path', t('Please, write file path.'));
        }
        if (empty($form_state['values']['path_line'])) {
          form_set_error('path_line', t('Please, write path line.'));
        }
        if (!empty($form_state['values']['file_path']) && !$exists) {
          form_set_error('file_path', t('Please, write correct file path or file is not exists.'));
        }
        if (!empty($form_state['values']['file_path']) && $exists && !$readable) {
          form_set_error('file_path', t('Сan not to read the file. Check the rights for reading.'));
        }
        break;
    }
  }
}

/**
 * Submit handler for project_version_admin_form().
 */
function project_version_admin_form_submit($form, &$form_state) {

  variable_set('project_version_option', $form_state['values']['options']);

  if ($form_state['values']['options'] == 0) {
    variable_set('project_version_custom', '');
    variable_set('project_version_file_path', '');
    variable_set('project_version_path_line', '');
  }
  else {
    variable_set('project_version_custom', $form_state['values']['custom_version']);
    variable_set('project_version_file_path', $form_state['values']['file_path']);
    variable_set('project_version_path_line', $form_state['values']['path_line']);

    // Ensure translations don't break during installation.
    $t = get_t();
    drupal_set_message($t("Now you can see project version " . l($t('here'), 'admin/reports/status')));
  }
}
